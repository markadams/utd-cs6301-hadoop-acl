package cs6301scc;

import cs6301scc.interfaces.IAuthenticator;
import java.io.*;
import java.security.*;
import java.util.*;
import java.math.*;


public class Authenticator implements IAuthenticator {
	private String fileName = "";

	public Authenticator ( String fName ){
		fileName = fName;
	}
	/**
	 * Authenticates a user by verifying the username and password. True if valid. False if not.
	 */
	@Override
	public boolean Authenticate(String username, String password) {
		try{
			MessageDigest msgdgst = MessageDigest.getInstance("SHA-256");
			String[] parameters = getUserLogin(username);
			if (parameters.length == 3 ){
				if (parameters[0].equals(username)){
					String ps = password+parameters[2];
					msgdgst.update(ps.getBytes());
					byte[] b_result = msgdgst.digest();
					String str_result = new BigInteger(1, b_result).toString(16);
					if (str_result.equals(parameters[1])){
						return true;
					}
					else {
						return false;
					}
				}
			} else { // no user found
				return false;
			}
		} catch (Exception e){
				System.out.println("An error occured in Authenticator");
		}
		return false;
	}
	private String[] getUserLogin ( String username ) throws IOException{
		Scanner input = new Scanner (new File (fileName));
		String[] userData = {};
		while (input.hasNext()){
			userData = input.nextLine().split(":");
			if (userData[0].equals(username)){
					break;
			}
		}
		return userData;
	}

}
