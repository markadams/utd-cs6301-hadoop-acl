package cs6301scc;

import cs6301scc.errors.ParsingError;
import cs6301scc.interfaces.IPigParser;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PigParser implements IPigParser {
	Pattern p = Pattern.compile("load '(.+)'");
	
	/**
	 * Parses a PIG script and returns back a list of files accessed by the script.
	 * @throws ParsingError 
	 */
	@Override
	public String[] Parse(String scriptContent) throws ParsingError {
		//format is in "A = load 'filename' as ...
		if (scriptContent.contains("load")){
			try {
				ArrayList<String> result = new ArrayList<String>();
				Matcher matches = p.matcher(scriptContent);
				
				while (matches.find()){
					result.add(matches.group(1));
				}
				
				String[] rtn = result.toArray(new String[result.size()]);
				return rtn;

			} catch (Exception e){
				throw new ParsingError("An error occured while parsing the Pig script.", e);
				
			}
		}
		else {
		 	return null;
		}
	}

}
