package cs6301scc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.pig.ExecType;
import org.apache.pig.PigServer;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.impl.logicalLayer.FrontendException;

import cs6301scc.interfaces.IJobRunner;



public class JobRunner implements IJobRunner {
	private Properties execProps;
	private Configuration config;
	
	public JobRunner(String hdfsConfString, String mapredConfString) {
		execProps = new Properties();
		execProps.setProperty("fs.default.name", hdfsConfString);
		execProps.setProperty("mapred.job.tracker", mapredConfString);
		
		config = new Configuration();
		config.setStrings("fs.default.name", hdfsConfString);
		config.setStrings("mapred.job.tracker", mapredConfString);
	}
	
	/**
	 * Runs the specified Pig script against a Hadoop cluster
	 */
	@Override
	public JobResult SubmitJob(String scriptContent) {

		UUID id = UUID.randomUUID();
		String id_base = "results-" + id.toString();
		String id_dir = "tmp/" + id_base;
		
		try {
			PigServer pigServer = new PigServer(ExecType.MAPREDUCE, execProps);
			FileSystem fs = FileSystem.get(config);
			
			
			pigServer.registerQuery(scriptContent);

			pigServer.store("results", id_dir);
			fs.copyToLocalFile(true, new Path(id_dir), new Path("/tmp/") );
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		JobResult result = new JobResult();
		result.ResultFile = new File("/tmp/" + id_base);
		result.ResultData = readFile(new File(result.ResultFile, "part-r-00000")); //TODO
		
		return result;
	}

	private static String readFile(File file) {
		try {
		  FileInputStream stream = new FileInputStream(file);
		  FileChannel fc = stream.getChannel();
		  MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
		  /* Instead of using default, pass in a decoder. */

		  String results = Charset.defaultCharset().decode(bb).toString();
		  stream.close();
		  
		  return results;
		  
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error while reading results file";
	}
	
	void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
		      delete(c);
		}
		f.delete();
	}
}
