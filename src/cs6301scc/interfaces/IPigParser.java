package cs6301scc.interfaces;

import cs6301scc.errors.ParsingError;

public interface IPigParser {
	public String[] Parse(String scriptContent) throws ParsingError;
}
