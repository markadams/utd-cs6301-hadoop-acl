package cs6301scc.interfaces;

import cs6301scc.JobResult;
import cs6301scc.errors.HadoopJobFailedException;

public interface IJobRunner {
	public JobResult SubmitJob(String scriptContent) throws HadoopJobFailedException;
}

