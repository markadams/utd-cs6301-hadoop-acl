package cs6301scc.interfaces;

public interface IAccessController {
	public void CheckAccess(String[] filenames, String username) throws SecurityException;
}
