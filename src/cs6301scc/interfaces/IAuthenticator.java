package cs6301scc.interfaces;

public interface IAuthenticator {
	public boolean Authenticate(String username, String password);
}
