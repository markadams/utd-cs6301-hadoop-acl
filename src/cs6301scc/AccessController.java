package cs6301scc;

import cs6301scc.interfaces.IAccessController;
import external.xml.XPDP;



public class AccessController implements IAccessController {
	private XPDP simplePDP;
	private String[] policyFiles;
	private String groupFile;
	
	public AccessController(String[] policyFiles, String groupFile){
		this.policyFiles = policyFiles;
		this.groupFile = groupFile;
	}
	
	/**
	 * Verifies that a user has access to the specified files. If not, throws an exception
	 */
	@Override
	public void CheckAccess(String[] filenames, String username) 
			throws SecurityException {
		// TODO Auto-generated method stub
		try {
			simplePDP = new XPDP(policyFiles);
			for (String name : filenames) {
				if (!userAccessFile(username, name))
					throw new SecurityException("User " + username
						+ " doesn't have access to file " + name);
			}
		} catch (SecurityException e){
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean userAccessFile(String username, String filename) {
		boolean [] result = new boolean[1];
		try {
			simplePDP.run(groupFile, filename, simplePDP, username, result);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result[0];
	}
}
