package cs6301scc.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.security.auth.Subject;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.Box;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Principal;

public class LoginWindow extends JFrame {
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JButton btnLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginWindow frame = new LoginWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		Component marginTop = Box.createVerticalStrut(50);
		GridBagConstraints gbc_marginTop = new GridBagConstraints();
		gbc_marginTop.gridwidth = 2;
		gbc_marginTop.insets = new Insets(0, 0, 5, 5);
		gbc_marginTop.gridx = 1;
		gbc_marginTop.gridy = 0;
		getContentPane().add(marginTop, gbc_marginTop);
		
		Component marginLeft = Box.createHorizontalStrut(100);
		GridBagConstraints gbc_marginLeft = new GridBagConstraints();
		gbc_marginLeft.insets = new Insets(0, 0, 5, 5);
		gbc_marginLeft.gridx = 0;
		gbc_marginLeft.gridy = 1;
		getContentPane().add(marginLeft, gbc_marginLeft);
		
		JLabel lblUsername = new JLabel("Username:");
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsername.anchor = GridBagConstraints.EAST;
		gbc_lblUsername.gridx = 1;
		gbc_lblUsername.gridy = 1;
		getContentPane().add(lblUsername, gbc_lblUsername);
		
		txtUsername = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		getContentPane().add(txtUsername, gbc_textField);
		txtUsername.setColumns(10);
		
		Component marginRight = Box.createHorizontalStrut(100);
		GridBagConstraints gbc_marginRight = new GridBagConstraints();
		gbc_marginRight.insets = new Insets(0, 0, 5, 0);
		gbc_marginRight.gridx = 3;
		gbc_marginRight.gridy = 1;
		getContentPane().add(marginRight, gbc_marginRight);
		
		JLabel lblPassword = new JLabel("Password:");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.gridx = 1;
		gbc_lblPassword.gridy = 2;
		getContentPane().add(lblPassword, gbc_lblPassword);
		
		txtPassword = new JPasswordField();
		GridBagConstraints gbc_txtPassword = new GridBagConstraints();
		gbc_txtPassword.insets = new Insets(0, 0, 5, 5);
		gbc_txtPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPassword.gridx = 2;
		gbc_txtPassword.gridy = 2;
		getContentPane().add(txtPassword, gbc_txtPassword);
		
		btnLogin = new JButton("Login");

		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.gridwidth = 2;
		gbc_btnLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogin.gridx = 1;
		gbc_btnLogin.gridy = 3;
		getContentPane().add(btnLogin, gbc_btnLogin);
		
		Component marginBottom = Box.createVerticalStrut(50);
		GridBagConstraints gbc_marginBottom = new GridBagConstraints();
		gbc_marginBottom.gridwidth = 2;
		gbc_marginBottom.insets = new Insets(0, 0, 0, 5);
		gbc_marginBottom.gridx = 1;
		gbc_marginBottom.gridy = 4;
		getContentPane().add(marginBottom, gbc_marginBottom);
		
		KeyListener enterPress = new KeyListener(){

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER){
					btnLogin.doClick();
				}
				
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
			
		};
		
		txtUsername.addKeyListener(enterPress);
		txtPassword.addKeyListener(enterPress);
	}
	
	public void addLoginActionListener(ActionListener listener){
		btnLogin.addActionListener(listener);
	}
	
	
	public String getUsername(){
		return txtUsername.getText();
	}
	
	public String getPassword(){
		return new String(txtPassword.getPassword());
	}
	
	public void resetPassword(){
		txtPassword.setText("");
		txtPassword.requestFocus();
	}
	
}
