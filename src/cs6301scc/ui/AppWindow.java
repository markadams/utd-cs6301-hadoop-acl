package cs6301scc.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.Box;
import java.awt.Insets;

import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JEditorPane;

public class AppWindow extends JFrame {

	private List<ActionListener> fileSelectedListeners = new LinkedList<ActionListener>();
	private List<ActionListener> runScriptListeners = new LinkedList<ActionListener>();
	
	private JPanel contentPane;
	private JTextField txtFilename;
	private JButton btnRunScript;
	private JEditorPane editorPane;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppWindow frame = new AppWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AppWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblPleaseHitBrowse = new JLabel("Please hit browse to select your Pig script");
		GridBagConstraints gbc_lblPleaseHitBrowse = new GridBagConstraints();
		gbc_lblPleaseHitBrowse.anchor = GridBagConstraints.WEST;
		gbc_lblPleaseHitBrowse.gridwidth = 5;
		gbc_lblPleaseHitBrowse.insets = new Insets(0, 0, 5, 5);
		gbc_lblPleaseHitBrowse.gridx = 1;
		gbc_lblPleaseHitBrowse.gridy = 1;
		contentPane.add(lblPleaseHitBrowse, gbc_lblPleaseHitBrowse);
		
		txtFilename = new JTextField();
		txtFilename.setEditable(false);
		GridBagConstraints gbc_txtFilename = new GridBagConstraints();
		gbc_txtFilename.gridwidth = 4;
		gbc_txtFilename.insets = new Insets(0, 0, 5, 5);
		gbc_txtFilename.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilename.gridx = 1;
		gbc_txtFilename.gridy = 2;
		contentPane.add(txtFilename, gbc_txtFilename);
		txtFilename.setColumns(10);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				int result = chooser.showDialog(AppWindow.this, "Select");
				
				if (result == JFileChooser.APPROVE_OPTION){
					txtFilename.setText(chooser.getSelectedFile().getPath());
					btnRunScript.setEnabled(true);
					
				}
				
				for (ActionListener listener : fileSelectedListeners){
					ActionEvent evt = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "fileSelected");
					listener.actionPerformed(evt);
				}
			}
		});
		GridBagConstraints gbc_btnBrowse = new GridBagConstraints();
		gbc_btnBrowse.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowse.gridx = 5;
		gbc_btnBrowse.gridy = 2;
		contentPane.add(btnBrowse, gbc_btnBrowse);
		
		Box horizontalBox = Box.createHorizontalBox();
		GridBagConstraints gbc_horizontalBox = new GridBagConstraints();
		gbc_horizontalBox.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalBox.gridwidth = 2;
		gbc_horizontalBox.gridx = 1;
		gbc_horizontalBox.gridy = 3;
		contentPane.add(horizontalBox, gbc_horizontalBox);
		
		btnRunScript = new JButton("Run Script");
		btnRunScript.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (ActionListener listener : runScriptListeners){
					ActionEvent evt = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "runScripts");
					listener.actionPerformed(evt);
				}
			}
		});
		
		btnRunScript.setEnabled(false);
		GridBagConstraints gbc_btnRunScript = new GridBagConstraints();
		gbc_btnRunScript.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRunScript.gridwidth = 5;
		gbc_btnRunScript.insets = new Insets(0, 0, 5, 5);
		gbc_btnRunScript.gridx = 1;
		gbc_btnRunScript.gridy = 4;
		contentPane.add(btnRunScript, gbc_btnRunScript);
		
		JLabel lblResults = new JLabel("Results");
		GridBagConstraints gbc_lblResults = new GridBagConstraints();
		gbc_lblResults.anchor = GridBagConstraints.WEST;
		gbc_lblResults.insets = new Insets(0, 0, 5, 5);
		gbc_lblResults.gridx = 1;
		gbc_lblResults.gridy = 6;
		contentPane.add(lblResults, gbc_lblResults);
		
		editorPane = new JEditorPane();
		editorPane.setEditable(false);
		GridBagConstraints gbc_editorPane = new GridBagConstraints();
		gbc_editorPane.gridwidth = 5;
		gbc_editorPane.insets = new Insets(0, 0, 0, 5);
		gbc_editorPane.fill = GridBagConstraints.BOTH;
		gbc_editorPane.gridx = 1;
		gbc_editorPane.gridy = 7;
		JScrollPane editorScroll = new JScrollPane(editorPane);
		contentPane.add(editorScroll, gbc_editorPane);
	}
	
	public void addFileSelectedListener(ActionListener listener){
		fileSelectedListeners.add(listener);
	}
	
	public void addRunScriptListenerListener(ActionListener listener){
		runScriptListeners.add(listener);
	}
	
	public File getSelectedFile(){
		if ((txtFilename.getText() != null) && (!txtFilename.getText().equals(""))){
			return new File(txtFilename.getText());
		}else{
			return null;
		}
		
	}
	
	public synchronized void setResultText(String value) {
		editorPane.setText(value);
	}
}
