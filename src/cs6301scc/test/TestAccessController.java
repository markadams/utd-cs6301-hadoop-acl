package cs6301scc.test;

import cs6301scc.interfaces.IAccessController;

public class TestAccessController implements IAccessController {
	
	protected boolean result;
	
	public TestAccessController(boolean result){
		this.result = result;
	}


	@Override
	public void CheckAccess(String[] filenames, String username)
			throws SecurityException {
		
		if (!result){
			throw new SecurityException("You don't have access to run this script.");
		}
		
	}

}
