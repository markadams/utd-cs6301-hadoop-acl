package cs6301scc.test;

import cs6301scc.interfaces.IAuthenticator;

public class TestAuthenticator implements IAuthenticator {

	private String username;
	
	public TestAuthenticator(String username){
		this.username = username;
	}
	
	@Override
	public boolean Authenticate(String username, String password) {
		if (username.equals(this.username)){
			return true;
		}else{
			return false;
		}
	}

}
