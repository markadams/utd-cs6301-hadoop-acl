package cs6301scc.test;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import cs6301scc.PigParser;
import cs6301scc.errors.ParsingError;

public class PigParserTest extends PigParser {

	PigParser parser;
	
	@Before
	public void before(){
		parser = new PigParser();
	}
	
	@Test
	public void testBasicFilenameParse() throws ParsingError {
		String[] results = parser.Parse("load 'awesomefile.txt';");
		
		assertArrayEquals(new String[]{"awesomefile.txt"}, results);
	}
	
	@Test
	public void testBasicFilenameParseWithAddlSuffix() throws ParsingError {
		String[] results = parser.Parse("load 'awesomefile.txt' USING X;");
		
		assertArrayEquals(new String[]{"awesomefile.txt"}, results);
	}
	
	@Test
	public void testMultilineBasicFilenameParse() throws ParsingError {
		String[] results = parser.Parse("load 'awesomefile.txt';\nload 'awesomefile2.txt';\n");
		
		assertArrayEquals(new String[]{"awesomefile.txt","awesomefile2.txt"}, results);
	}

}
