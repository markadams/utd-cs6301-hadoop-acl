package cs6301scc.test;

import java.io.File;

import cs6301scc.JobResult;
import cs6301scc.interfaces.IJobRunner;

public class TestJobRunner implements IJobRunner{

	@Override
	public JobResult SubmitJob(String scriptContent) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {

		}
		
		JobResult result = new JobResult();
		result.ResultData = "These are the final results";
		result.ResultFile = new File("/tmp/path/to/file");
		
;		return result;
	}

}
