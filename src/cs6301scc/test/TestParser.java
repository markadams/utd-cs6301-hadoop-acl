package cs6301scc.test;

import java.io.FileNotFoundException;

import cs6301scc.errors.ParsingError;
import cs6301scc.interfaces.IPigParser;

public class TestParser implements IPigParser {

	protected boolean throwError;
	
	public TestParser(boolean throwError){
		this.throwError = throwError;
	}
	
	@Override
	public String[] Parse(String scriptContent) throws ParsingError {
		if (throwError){
			throw new ParsingError("The file couldn't be parsed.", new FileNotFoundException());
		}
		
		return new String[] { "students", "faculty" };
	}
	
}
