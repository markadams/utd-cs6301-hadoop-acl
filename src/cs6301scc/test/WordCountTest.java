package cs6301scc.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.pig.builtin.TOKENIZE;

public class WordCountTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		List<String> words = getWords(args[0]);
		Hashtable<String, Integer> wordCounts = new Hashtable<String,Integer>();
		
		for (Iterator<String> i = words.iterator(); i.hasNext();){
			String word = i.next();

			if (wordCounts.containsKey(word)){
				wordCounts.put(word, wordCounts.get(word) + 1);
			}else{
				wordCounts.put(word, 1);
			}
		}
		
		LinkedList<Entry<String, Integer>> wordList = new LinkedList<Entry<String, Integer>>(wordCounts.entrySet());
		
		Collections.sort(wordList, new Comparator<Entry<String,Integer>>() {

			@Override
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				return o2.getValue() - o1.getValue(); 
			}


		});
		
		for (Iterator<Entry<String, Integer>> i = wordList.iterator(); i.hasNext();){
			Entry<String, Integer> value = i.next();
			System.out.println(value.getKey() + ':' + value.getValue().toString());
			
		}
			
			
			
			

	}
	
	public static List<String> getWords(String filename) throws IOException{
		
		
		String delim = " \",()*";
		LinkedList<String> list = new LinkedList<String>();
		
		BufferedReader br = new BufferedReader(new FileReader(filename));
		
		String line = null;
		
		while ((line = br.readLine()) != null){
			StringTokenizer tok = new StringTokenizer(line, delim, false);
			
			while (tok.hasMoreTokens()) {
				list.add(tok.nextToken());
			}
		}
		
		
		return list;
	}

}
