package cs6301scc.errors;

/**
 * Exception to be thrown whenever parsing of a Pig script fails
 * @author mark
 *
 */
public class ParsingError extends Exception {
	public ParsingError(String message, Exception innerException){
		super(message, innerException);
	}
}
