package cs6301scc.errors;

/**
 * Exception to be thrown whenever a Hadoop job fails
 * @author mark
 *
 */
public class HadoopJobFailedException extends Exception {
	public HadoopJobFailedException(String message, Exception cause){
		super(message, cause);
	}
}
