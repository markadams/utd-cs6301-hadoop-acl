package cs6301scc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.EventHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.sun.org.apache.bcel.internal.generic.Type;

import cs6301scc.errors.HadoopJobFailedException;
import cs6301scc.errors.ParsingError;
import cs6301scc.interfaces.IAccessController;
import cs6301scc.interfaces.IAuthenticator;
import cs6301scc.interfaces.IJobRunner;
import cs6301scc.interfaces.IPigParser;
import cs6301scc.test.TestAccessController;
import cs6301scc.test.TestAuthenticator;
import cs6301scc.test.TestParser;
import cs6301scc.ui.AppWindow;
import cs6301scc.ui.LoginWindow;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Main {

	public final boolean TEST_MODE = false;
	
	public JFrame currentWindow;
	public AppState parameters = new AppState();
	
	public IAuthenticator authenticator;
	public IPigParser parser;
	public IAccessController accessController;
	public IJobRunner runner;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Configuration config = GetConfiguration();

			Main app = new Main();
			
			// TODO Auto-generated method stub
			app.ConfigureDependencies(config);
			app.ShowLoginWindow();
			
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static PropertiesConfiguration GetConfiguration() throws ConfigurationException{
		return new PropertiesConfiguration("application.properties");
	}
	
	/***
	 * Configures dependent components
	 */
	public void ConfigureDependencies(Configuration config){
		authenticator = new Authenticator(config.getString("auth.passdb"));
		
		parser = new PigParser();
		
		accessController = new AccessController(
				config.getStringArray("policy.filename"), config.getString("groups.filename"));
		
		runner = new JobRunner(
				config.getString("hadoop.hdfs"), 
				config.getString("hadoop.mapred")
				);
		
		if (TEST_MODE){
			
			/* Mock objects */
			authenticator = new cs6301scc.test.TestAuthenticator("test");
			parser = new cs6301scc.test.TestParser(false);
			accessController = new cs6301scc.test.TestAccessController(true);
			runner = new cs6301scc.test.TestJobRunner();
			
		}
		
	}
	
	/**
	 * Closes whatever the currently open window is
	 */
	public void CloseCurrentWindow(){
		currentWindow.setVisible(false);
		currentWindow = null;
	}

	/**
	 * Shows the login window and begins the login workflow
	 */
	public void ShowLoginWindow(){
		LoginWindow login = new LoginWindow();
		
		login.addLoginActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				LoginWindow window = (LoginWindow)currentWindow;
				String username = window.getUsername();
				String password = window.getPassword();
				
				if (authenticator.Authenticate(username,password)){
					parameters.Username = username;
					
					CloseCurrentWindow();
					ShowAppWindow();
					
				}else{
					JOptionPane.showMessageDialog(window, "Uh oh! It looks like your credentials weren't correct. Please try again.");
					window.resetPassword();
				}
				
			}
		});
		
		currentWindow = login;
		currentWindow.setVisible(true);
	}

	/**
	 * Shows the main application window after authentication
	 */
	public void ShowAppWindow(){
		AppWindow appWindow = new AppWindow();
		
		// Add the handler to update the model when a file is selected.
		appWindow.addFileSelectedListener((ActionListener)EventHandler.create(ActionListener.class, this, "GetSelectedFile"));
		
		// Add the handler for when the user attempts to run the script.
		appWindow.addRunScriptListenerListener((ActionListener)EventHandler.create(ActionListener.class, this, "ProcessScript"));
		
		currentWindow = appWindow;	
		currentWindow.setVisible(true);
	}

	/**
	 * Handler for file selection from AppWindow
	 */
	public void GetSelectedFile(){
		AppWindow appWindow = (AppWindow) currentWindow;
		parameters.PigScriptFile = appWindow.getSelectedFile();
	}
	
	/**
	 * Reads the Pig script from the filesystem and returns the result as a string
	 * @param pigScript The Pig script file
	 * @return The content of the Pig script file
	 * @throws IOException
	 */
	public String ReadPigScript(File pigScript) throws IOException{
		
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(pigScript));
		
		String line;
		
		while ((line = reader.readLine()) != null){
			sb.append(line);
			sb.append('\n');
		}
		
		reader.close();
		
		return sb.toString();
		
	}
	
	/**
	 * Attempts to verify privileges and run the Pig script.
	 */
	public void ProcessScript(){
		
		AppWindow appWindow = (AppWindow) currentWindow;
		
		try {
			// Load the Pig script and parse for filenames
			parameters.PigScriptContent = ReadPigScript(parameters.PigScriptFile);
			parameters.Filenames = parser.Parse(parameters.PigScriptContent);
			
			// Verify access
			accessController.CheckAccess(parameters.Filenames, parameters.Username);
			
			appWindow.setResultText(String.format("Job is now running. Started at %s.", GetCurrentIsoDate()));
			
			// Run the job on a separate thread so we don't block the UI
			Thread t = new Thread(new Runnable(){

				@Override
				public void run() {
					AppWindow appWindow = (AppWindow) currentWindow;
					
					
					try {
						// Submit the job to the job runner
						JobResult result = runner.SubmitJob(parameters.PigScriptContent);
						appWindow.setResultText(result.ResultData);
					} catch (HadoopJobFailedException e) {
						// If we get any errors while running the job, show them here.
						appWindow.setResultText(String.format("ERROR: %s", e.getMessage()));
					}
					
				}
				
			});
			
			t.start();
		}
		catch (SecurityException e){
			JOptionPane.showMessageDialog(currentWindow, e.getMessage(), "Security Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(currentWindow, e.getMessage(), "I/O Error", JOptionPane.ERROR_MESSAGE);
		} catch (ParsingError e) {
			JOptionPane.showMessageDialog(currentWindow, e.getMessage(), "Parser Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Returns the current date in ISO format
	 * @return
	 */
	public static String GetCurrentIsoDate(){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'HH:mmZ");
		return df.format(new Date());
	}
}
