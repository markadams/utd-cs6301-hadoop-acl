#!/bin/bash
str="$1:";
rand=$RANDOM;
hash=`echo -n "$2$rand" | sha256sum | cut -d " " -f1`;
echo $str$hash":"$rand >> $3;

