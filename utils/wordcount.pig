A1 = load '/data/a.txt';
A2 = load '/data/b.txt';
A  = union A1, A2;
B = foreach A generate flatten(TOKENIZE((chararray)$0)) as word;
C = group B by word;
F = foreach C generate COUNT(B), group;
results = order F by $0 DESC;
